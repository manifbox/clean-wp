module.exports = function(grunt) {

    grunt.initConfig({

        // let us know if our JS is sound
        jshint: {
            options: {
                "bitwise": true,
                "browser": true,
                "curly": true,
                "eqeqeq": true,
                "eqnull": true,
                "es5": false,
                "esnext": true,
                "immed": true,
                "jquery": true,
                "latedef": true,
                "newcap": true,
                "noarg": true,
                "node": true,
                "strict": false,
                "trailing": true,
                "undef": true,
                "globals": {
                  "jQuery": true,
                  "themeUrl": true,
                  "siteUrl": true,
                  "assetsUrl": true,
                  "lightbox": true,
                  "alert": true
                }
            },
            all: [
                'Gruntfile.js',
                'dev/assets/js/source/general.js'
            ]
        },

        // concatenation and minification all in one
        uglify: {
            dist: {
                files: {
                    'dev/assets/js/build/vendor.min.js': [
                        'dev/assets/js/vendor/jquery/jquery-3.1.0.min.js',
                        'dev/assets/js/vendor/modernizr/modernizr-build.js'
                    ],
                    'dev/assets/js/build/script.min.js': [
                      'dev/assets/js/vendor/foundation/foundation.min.js',
                      //'dev/assets/js/vendor/slick/slick.js',
                      //'dev/assets/js/vendor/inview/inview.js',
                      //'dev/assets/js/vendor/lazysizes/lazysizes.js',
                      //'dev/assets/js/vendor/picturefill/picturefill.js',
                      'dev/assets/js/source/general.js'
                    ]
                }
            }
        },

        // style (Sass) compilation via Compass
        compass: {
            dist: {
                options: {
                    sassDir: 'dev/assets/css/source',
                    cssDir: 'dev/assets/css/build',
                    imagesDir: 'dev/assets/images',
                    images: 'dev/assets/images',
                    javascriptsDir: 'dev/assets/js/build',
                    environment: 'production',
                    //outputStyle: 'expanded',
                    outputStyle: 'compressed',
                    relativeAssets: true,
                    noLineComments: true,
                    force: true
                }
            }
        },

        // watch our project for changes
        watch: {
            options : {
              livereload : true
            },
            compass: {
                files: [
                    'dev/assets/css/source/*',
                    'dev/assets/css/source/**/*',
                    'dev/assets/css/vendor/*',
                    'dev/assets/css/vendor/**/*'
                ],
                tasks: ['compass', 'uglify', 'cachebuster']
            },
            js: {
                files: [
                    '<%= jshint.all %>'
                ],
                tasks: ['jshint', 'uglify']
            }
        },
        imagemin: {
           dist: {
              options: {
                optimizationLevel: 8
              },
              files: [{
                 expand: true,
                 cwd: 'dev/assets/uncompressed-images',
                 src: ['**/*.{png,svg,jpg,gif}'],
                 dest: 'dev/assets/images/'
              }]
           }
        },
        htmlclean: {
          options: {
          edit: function(html) { return html.replace(/<script src="\/\/localhost:35729\/livereload.js"><\/script>/g, "''"); }
          },
          deploy: {
            expand: true,
            cwd: 'dev/',
            src: '**/*.php',
            dest: 'build/'
          }
        },
        cachebuster: {
          build: {
              options: {
                basedir: '',
                format: 'json',
                banner:'',
                complete: function(hashes) {
                    return {
                        md5: hashes
                    };
                }
              },
              src: [ 'dev/assets/css/build/style.css', 'dev/assets/js/build/script.min.js', 'dev/assets/js/build/vendor.min.js' ],
              dest: 'dev/assets/cachebust/cachebusters.json'
          }
        }


    });

    // load tasks
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-htmlclean');
    grunt.loadNpmTasks('grunt-cachebuster');

    // register task
    grunt.registerTask('compile', ['imagemin']);
    grunt.registerTask('compress', ['htmlclean']);

    grunt.registerTask('default', [
        'jshint',
        'cachebuster',
        'compass',
        'uglify',
        'watch'
    ]);

};
