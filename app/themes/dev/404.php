<?php
get_header();
?>
<section id="page-content" class="lp-row">

		<div class="row">
				<div class="columns large-12">
					<h1>Oops. Cette page n'existe plus</h1>
					<a href="<?php bloginfo('wpurl'); ?>" class="fourofour">Cliquez ici pour retourner vers l'accueil</a></div>
    </div>

</section>

<?php get_footer(); ?>
