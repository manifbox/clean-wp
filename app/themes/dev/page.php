<?php
get_header();
global $assetsDir;

if(have_posts()) {
	while(have_posts()) {
		the_post();

		 $pageContent = apply_filters('the_content', get_the_content());

		echo '<div id="global-wrapper">
		  <h1 id="main-ttl">'.get_the_title().'</h1>
		  <div id="main-content">'.$pageContent.'</div>
		</div>';
	}
}

get_footer();
?>
