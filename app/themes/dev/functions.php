<?php
add_theme_support('post-thumbnails');
add_theme_support('menus');

add_image_size( 'medium', 300, 300, true );

// auto update plugins
add_filter( 'auto_update_plugin', '__return_true' );

// remove admin bar
show_admin_bar( false );

/*===================================================================================
* Global variables
* =================================================================================*/
$siteUrl = get_bloginfo('wpurl');
$assetsDir = get_bloginfo('template_directory').'/assets';
$p_id = get_the_ID();
//$clang = qtranxf_getLanguage();

/*===================================================================================
* ADMIN CSS STYLE
* =================================================================================*/
add_action('admin_head', 'my_custom_fonts');
function my_custom_fonts() {
  echo '<style>
    .adn-tbl-prod-specs {
      width: 100%;
    }
    .adn-tbl-prod-specs td {
      padding: 1em 0.5em;
      width: 25%;
      border: solid 1px #f4f4f4;
      vertical-align: top;
    }
    .adn-tbl-prod-specs td.hd-desc {
      border-top: solid 1px #000000;
      background: #f4f4f4;
      color: #000;
      text-align: center;
    }
    .adn-tbl-prod-specs td label {
      background: #f4f4f4;
      padding: 0.5em 0;
      color: #000;
      width: 100%;
      display: block;
      text-align: center;
    }
    .adn-tbl-prod-specs td select,
    .adn-tbl-prod-specs td input,
    .adn-tbl-prod-specs td textarea {
      width: 100%;
      display: block;
    }
    .adn-tbl-ttl {
      background: #23282d;
      color: #ffffff;
      border-bottom: solid 1px #fff;
      padding: 10px;
      font: bold 14px/1.25 "Open Sans",sans-serif;
    }
    .adn-tbl-prod-specs td textarea {
      min-height: 150px;
    }
    .adn-tbl-prod-specs .hd-promo {
      background: #BA1F3A; color: #ffffff;
    }
    /* hide ADD NEW CATEGORY for terms. Force them to go to actual page */
    .wp-hidden-children {
      display: none;
    }
    .cstm-button {
      display: block;
      max-width: 200px;
      border: none;
      margin: 1em auto; padding: 1em;
      background: #e14d43;
      color: #ffffff;
      text-align: center;
      text-transform: uppercase;
      letter-spacing: 1px;
      font-size: 0.75em;
    }
    .preview-ambiance {
      max-width: 100%;
    }
  </style>';
}

  /*===================================================================================
  * automatically check parent category if child
  * =================================================================================*/
  function super_category_toggler() {

    $taxonomies = apply_filters('super_category_toggler',array());
    for($x=0;$x<count($taxonomies);$x++)
    {
      $taxonomies[$x] = '#'.$taxonomies[$x].'div .selectit input';
    }
    $selector = implode(',',$taxonomies);
    if($selector == '') $selector = '.selectit input';

    echo '
    <script>
    jQuery("'.$selector.'").change(function(){
      var $chk = jQuery(this);
      var ischecked = $chk.is(":checked");
      $chk.parent().parent().siblings().children("label").children("input").each(function(){
        var b = this.checked;
        ischecked = ischecked || b;
      })
      checkParentNodes(ischecked, $chk);
    });
    function checkParentNodes(b, $obj)
    {
      $prt = findParentObj($obj);
      if ($prt.length != 0)
      {
        $prt[0].checked = b;
        checkParentNodes(b, $prt);
      }
    }
    function findParentObj($obj)
    {
      return $obj.parent().parent().parent().prev().children("input");
    }
    </script>
    ';

  }
  add_action('admin_footer', 'super_category_toggler');

/*===================================================================================
* Custom post types
* =================================================================================*/
add_action( 'init', 'build_taxonomies', 0 );

function build_taxonomies() {
  register_taxonomy(
    'manger',
    'menu',
    array(
      'hierarchical' => true,
      'label' => 'Manger',
      'query_var' => 'manger',
      'rewrite' => true
    )
  );

}

add_action( 'init', 'create_post_type' );
function create_post_type() {
  // artists
  register_post_type( 'custom_pst_typ',
  array(
    'labels' => array(
      'name' => __( 'CUSTOM' ),
      'singular_name' => __( 'CUSTOM' )
    ),
    'supports' => array(
      'title',
      'editor',
      'thumbnail'
    ),
    'taxonomies' => array('manger'),
    'public' => true,
    'menu_icon' => 'dashicons-store',
    'menu_position' => 5,
    'hierarchical' => false,
    'rewrite'   => array( 'slug' => '', 'with_front' => true ),
    'has_archive' => true
    )
  );
}
