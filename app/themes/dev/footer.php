<?php
global $assetsDir;
global $siteUrl;

global $iconHeart;
global $iconClose;

$Aurl = $assetsDir.'/cachebust/cachebusters.json';
$jsonContent = file_get_contents($Aurl);
$CacheBusterJson = json_decode($jsonContent, true);
$cbScript = $CacheBusterJson['md5']['dev/assets/js/build/script.min.js'];
?>
	<footer id="ftr">

		<div class="row">
			<div class="columns large-3"></div>
			<div class="columns large-3"></div>
			<div class="columns large-3"></div>
		</div>

	</footer>

	<?php wp_footer(); ?>

	<script src="<?php echo $assetsDir; ?>/js/build/script.min.js?v=<?php echo $cbScript; ?>"></script>
	<script src="//localhost:35729/livereload.js"></script>
</body>
</html>
