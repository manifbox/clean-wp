<?php

  // Live Server Database Settings

  define( 'DB_NAME',     'sommets');
  define( 'DB_USER',     'root');
  define( 'DB_PASSWORD', 'root' );
  define( 'DB_HOST',     'localhost'  );

  // Overwrites the database to save keep edeting the DB

  define('WP_HOME','http://club3sommets.mani/');
  define('WP_SITEURL','http://club3sommets.mani/');

  // Turn Debug off on live server
  define('WP_DEBUG', true);
  define('WP_DEBUG_LOG', false);
  define('WP_DEBUG_DISPLAY', true);
  define('WP_MEMORY_LIMIT', '256M');

?>
