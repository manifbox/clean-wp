<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


/** Use these settings on the local server */

if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
	include( dirname( __FILE__ ) . '/wp-config-local.php' );

} else {

	// ** MySQL settings ** //
	define( 'DB_NAME',     'sommets');
	define( 'DB_USER',     'root');
	define( 'DB_PASSWORD', 'root' );
	define( 'DB_HOST',     'localhost'  );

  	// ** Turn off the debugs on live except logs. */
	define('WP_DEBUG', false);
	define('WP_DEBUG_LOG', true);
	define('WP_DEBUG_DISPLAY', false);
	define('WP_MEMORY_LIMIT', '512M');
}


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'dE]DZahR#t3-H4Fv0KR 2lu;!dLcobH0d:?pgg=gex>p^>6OcN_)]0Psfr3A9@3(' );
define( 'SECURE_AUTH_KEY',  'MUBPTcX5Z2H%DUK)S.,tDEX`mbbB+acv-0{T,Owh@q)I=AyBNlnfx& 9Vv+p3cGl' );
define( 'LOGGED_IN_KEY',    '46za9+&#<Y/7P*gc#,x.ghi2Md*Ncp<sU^G{L&-6I2.hl$d8XkC>5aUWsr&QG6YZ' );
define( 'NONCE_KEY',        '<P:Y>!9;zMfus!~!@6C,3fz$]VBqH!(iz`Y!ink)-$g>^.3^*_ysvHmX.z^ Hbh|' );
define( 'AUTH_SALT',        '0eP8B{w~K#Zss085,/|)gD*]S0y+?dKGi}j3,Sb.G:%4=)1x?Y.F%kcbPeu)`F>f' );
define( 'SECURE_AUTH_SALT', 'UqJUpalz3F-4Oa<{n*~~e},1B}pv;e1l`;^V}qQZDRjj t^<q0KEGu1mm~86kGA?' );
define( 'LOGGED_IN_SALT',   '$apFXHeXjg&G;ve%H3e%e(qeXw,nTxKcB+w?%]bAmP_23%A:VHJ|sWk}R1c=oz$ ' );
define( 'NONCE_SALT',       ')OB*S$>GR_U|sBJ9R#RoW@d%Lo^dGCO5NFRx^6E@,5NdcztN3_!_ y<%gZsi[^@!' );


/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Change wp-content folder name (pretty, security, etc) */ 
define ('WP_CONTENT_FOLDERNAME', 'app');
define ('WP_CONTENT_DIR', ABSPATH . WP_CONTENT_FOLDERNAME) ;
define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/');
define('WP_CONTENT_URL', WP_SITEURL . WP_CONTENT_FOLDERNAME);

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
